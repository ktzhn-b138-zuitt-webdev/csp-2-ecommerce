const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");


// Create new Order (non-admin)
router.post("/checkout", auth.verify, (req, res) => {
	const user = auth.decode (req.headers.authorization);
	orderController.newOrder(user,req.body).then(resultFromController => res.send(resultFromController));
})


// Retrieve ALL orders (ADMIN)
router.get("/orders", auth.verify, (req, res) => {
	const user = auth.decode (req.headers.authorization);
	orderController.allOrder(user,req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	const user = auth.decode (req.headers.authorization);
	orderController.userOrder(user, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
