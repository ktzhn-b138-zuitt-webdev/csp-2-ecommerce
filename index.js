const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

const app = express();

// Database Connection 
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://dbKristenTan:APTcxPmkgr05VmxW@wdc028-course-booking.mkooj.mongodb.net/ecommerce_db?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//  top level routes 

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/users", orderRoutes);

//  top level routes  END

// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})

